<?php

namespace Nitra\AccessoriesBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 */
class Accessorie
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @MongoDB\Field(type="string")
     */
    private $name;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    private $category;
    
     /**
     * @MongoDB\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Brand")
     */
    private $brand;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Nitra\ProductBundle\Document\Product")
     */
    private $accessories;
    
    public function __construct()
    {
        $this->accessories = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category
     *
     * @param Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     *
     * @return Nitra\ProductBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brand
     *
     * @param Nitra\ProductBundle\Document\Brand $brand
     * @return self
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     *
     * @return Nitra\ProductBundle\Document\Brand $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Add accessory
     *
     * @param Nitra\ProductBundle\Document\Product $accessory
     */
    public function addAccessory(\Nitra\ProductBundle\Document\Product $accessory)
    {
        $this->accessories[] = $accessory;
    }

    /**
     * Remove accessory
     *
     * @param Nitra\ProductBundle\Document\Product $accessory
     */
    public function removeAccessory(\Nitra\ProductBundle\Document\Product $accessory)
    {
        $this->accessories->removeElement($accessory);
    }

    /**
     * Get accessories
     *
     * @return Doctrine\Common\Collections\Collection $accessories
     */
    public function getAccessories()
    {
        return $this->accessories;
    }
}
