# AccessorieBundle

## Описание
Данный бандл предназначен для работы с аксессуарами (Accessorie) - вся информация об аксессуарах
## AccessoriesController
* carouselAccessorizeProductsAction - карусель товаров
* getAccessories - возвращает товары по заданной категории и бренду 

## Подключение
Для подключения данного модуля в проект необходимо:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        ""e-commerce-site/accessoriesbundle": "dev-master",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\AccessoriesBundle\NitraAccessoriesBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```
