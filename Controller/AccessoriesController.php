<?php

namespace Nitra\AccessoriesBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AccessoriesController extends NitraController
{
    /**
     * Карусель аксессуаров
     *
     * @Template("NitraProductBundle:Carousels:products.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Product     $product        Product instance
     * @param string                                    $orientation    Carousel orientation
     * @param integer                                   $visible        Amount of visible carousel items
     * @param string                                    $imageSize      Imagine filter name
     * @param string                                    $class          Carousel class
     * @param string                                    $caption        Carousel caption
     * @param string                                    $type           Carousel type
     *
     * @return array Template parameters
     */
    public function carouselAccessoriesProductsAction($product, $orientation, $visible = 7, $imageSize = 'big_carousel_thumb', $class = 'accessorize_products', $caption = '', $type = 'simple')
    {
        // get accessories from product
        $accessories = $this->collectAccessoriesIdsFromProduct($product);
        // get categories tree (parents)
        $categories = $this->getParentCategoriesIds($product->getModel()->getCategory());
        // find collections of accessories
        $collections = $this->getAccessories($categories, $product->getModel()->getBrand()->getId());

        // each by collections
        foreach ($collections as $collection) {
            // each by accessories from even collection
            foreach ($collection->getAccessories() as $accessory) {
                // add accessory id to accessories
                $accessories[$accessory->getId()] = $accessory->getId();
            }
        }

        // get accessories by ids
        $products = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->field('id')->in(array_values($accessories))
            ->getQuery()->execute();

        return array(
            'items'       => $products,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
        );
    }

    /**
     * Collect accessories ids from product
     *
     * @param \Nitra\ProductBundle\Document\Product     $product    Product instance
     *
     * @return array Ids of accessories
     */
    protected function collectAccessoriesIdsFromProduct($product)
    {
        // define result array
        $productsIds = array();
        // each by product accessories
        foreach ($product->getAccessories() as $accessory) {
            // add accessory id to result array
            $productsIds[$accessory->getId()] = $accessory->getId();
        }

        // return result
        return $productsIds;
    }

    /**
     * Recursive get parent categories ids
     *
     * @param \Nitra\ProductBundle\Document\Category    $category   Category instance
     * @param array                                     $ids        List if categories ids
     *
     * @return array Ids of categories
     */
    protected function getParentCategoriesIds($category, $ids = array())
    {
        // add her id to result array
        $ids[] = $category->getId();

        return $category->getParent()
            // if category has parent - return result of this function with parent category argument
            ? $this->getParentCategoriesIds($category->getParent(), $ids)
            // else - return ids array
            : $ids;
    }

    /**
     * Get accessories by categories and brands
     *
     * @param array     $categoriesIds  List of categories ids
     * @param array     $brandId        List of brands ids
     * @param integer   $i              Index
     *
     * @return \Nitra\AccessoriesBundle\Document\Accessorie[]
     */
    protected function getAccessories($categoriesIds, $brandId, $i = 0)
    {
        // if category by key not exists
        if (!array_key_exists($i, $categoriesIds)) {
            return array();
        }

        // create query
        $query = $this->getDocumentManager()
            ->createQueryBuilder('NitraAccessoriesBundle:Accessorie')
            // add category condition
            ->field('category.id')->equals($categoriesIds[$i]);

        // add brand condition
        $this->appendBrandCondition($query, $brandId);

        // execute query
        $accessoriesCollection = $query->getQuery()->execute();

        // if accessories not found
        if (!$accessoriesCollection->count()) {
            // if brand id not empty
            if ($brandId) {
                // create expression for accessories collections without brand
                $expr = $query->expr()->field('brand')->exists(false);
                // add expression tp query
                $query->addOr($expr);
                // execute query
                $accessoriesCollection = $query->getQuery()->execute();
            }
            // if now too accessories not found
            if (!$accessoriesCollection->count()) {
                // return accessories for parent category
                return $this->getAccessories($categoriesIds, $brandId, $i + 1);
            }
        }

        // return accessories
        return $accessoriesCollection;
    }

    /**
     * Add brand condition to query
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $query      Query builder
     * @param string                                $brandId    Brand identifier
     */
    protected function appendBrandCondition($query, $brandId)
    {
        // create expression
        $expr = $query->expr();
        // if brand id not empty
        if ($brandId) {
            // add equals condition
            $expr->field('brand.id')->equals($brandId);
        } else {
            // add condition of brand for accessories not specified
            $expr->field('brand')->exists(false);
        }

        // add or condition to query
        $query->addOr($expr);
    }
}